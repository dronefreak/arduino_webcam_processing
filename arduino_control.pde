import processing.serial.*;
import cc.arduino.*;

Arduino arduino;
int ledPin = 8;

void setup()
{
  println(Arduino.list());
  arduino = new Arduino(this, Arduino.list()[8], 57600);
  arduino.pinMode(ledPin, Arduino.OUTPUT);
}

void draw()
{
  for(int i = 0; i<150; i++)
  {
    arduino.analogWrite(ledPin, i);
  }
  
  arduino.digitalWrite(ledPin, Arduino.HIGH);
  delay(100);
  arduino.digitalWrite(ledPin, Arduino.LOW);
}


